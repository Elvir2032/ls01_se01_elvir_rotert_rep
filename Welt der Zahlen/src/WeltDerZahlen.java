/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Elvir Rotert >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstraße
    double anzahlSterne = 2.0E11;
    
    // Wie viele Einwohner hat Berlin?
    float bewohnerBerlin = 3.769E6f;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 6775;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    double gewichtKilogramm = 200.000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    double flaecheGroessteLand = 17.1E6;
    
    // Wie groß ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Bewohner Berlins: " + bewohnerBerlin);
    
    System.out.println("So viele Tage bin ich alt: " + alterTage);
    
    System.out.printf("%s%.6s%n", "Gewicht schwerstes Tier der Welt in Tonnen: ", gewichtKilogramm);
    
    System.out.println("Fläche größtes Land der Welt: " + flaecheGroessteLand);
    
    System.out.println("Fläche kleinstes Land der Welt: " + flaecheKleinsteLand);  
            
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

