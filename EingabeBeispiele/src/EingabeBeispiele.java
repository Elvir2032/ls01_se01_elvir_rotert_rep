import java.util.Scanner;

public class EingabeBeispiele {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Geben Sie bitte eine Zahl ein: ");
		int zahl1 = myScanner.nextInt();
		System.out.println("Ihre Eingabe ist: " + zahl1);
		System.out.println("Geben Sie bitte Ihren Vornamen ein: ");
		String vorname = myScanner.next();
		System.out.println("Ihr Vorname ist: " + vorname);
		System.out.println("Geben Sie einen zufälligen Buchstaben ein: ");
		char buchstabe = myScanner.next().charAt(0);
		System.out.println("Der von Ihnen gewählte Buchstabe ist: " + buchstabe);
		
	}

}
