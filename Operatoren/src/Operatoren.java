﻿/* Operatoren.java
   Uebung zu Operatoren in Java
   @author
   @version
*/
public class Operatoren {
  public static void main(String [] args){
    /* 1. Vereinbaren Sie zwei Ganzzahlen.*/

	  int zahl1, zahl2;
	  
    System.out.println("ÜBUNG ZU OPERATOREN IN JAVA\n");
    
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */

    zahl1 = 75;
    zahl2 = 23;
    System.out.println("Zahl 1: " + zahl1);
    System.out.println("Zahl 2: " + zahl2);
    
    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */

    int erg = zahl1 + zahl2;
    System.out.println("Addition der Zahlen: " + erg);
    
    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */

    int erg2 = zahl1 - zahl2;
    int erg3 = zahl1 * zahl2;
    int erg4 = zahl1 / zahl2;
    System.out.println("Subtraktion der Zahlen: " + erg2);
    System.out.println("Multiplikation der Zahlen: " + erg3);
    System.out.println("Division der Zahlen: " + erg4);
    
    /* 5. Ueberprüfen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */

    boolean erg5 = zahl1 == zahl2;
    System.out.println("Sind die Zahlen gleich? " + erg5);
    
    /* 6. Wenden Sie die drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */

    boolean erg6 = zahl1 < zahl2;
    boolean erg7 = zahl1 > zahl2;
    boolean erg8 = zahl1 != zahl2;
    System.out.println("Ist Zahl 1 kleiner als Zahl 2? " + erg6);
    System.out.println("Ist Zahl 1 größer als Zahl 2? " + erg7);
    System.out.println("Sind die Zahlen ungleich? " + erg8);
    
    /* 7. Ueberprüfen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    
    boolean erg9 = (zahl1 < 50 && zahl1 > 0); 
    boolean erg10 = (zahl2 < 50 && zahl2 > 0);
    System.out.printf("%s%n%s%s%n%s%s", "Die Zahlen liegen im Intervall zwischen 0 und 50: ", "Zahl 1: ", erg9 , "Zahl 2: ", erg10);
    
  }//main
}// Operatoren
