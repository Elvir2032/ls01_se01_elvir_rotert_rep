package temperatur;

import java.util.Scanner;

public class temperatur {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Bitte den Startwert in Celcius eingeben: ");
        double start = scan.nextDouble();

        System.out.print("Bitte den Endwert in Celcius eingeben: ");
        double end = scan.nextDouble();

        System.out.print("Bitte die Schrittweite in Celcius eingeben: ");
        int step = scan.nextInt();

        double f = 0;

        if(start < end) {

            while(start <= end) {
                f = (start * 9.0 / 5.0) + 32.0;
                System.out.println(start + "�C\t" + f + "�F");
                start +=step;
            }

        } else {

            while(start >= end) {
                f = (start * 9.0 / 5.0) + 32.0;
                System.out.println(start + "�C\t" + f + "�F");
                start -=step;
            }

        }
    }

}