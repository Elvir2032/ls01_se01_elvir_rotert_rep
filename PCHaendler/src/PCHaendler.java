import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String artikel;
		int anzahl;
		double nettopreis;
		double mwst;
		double nettogesamtpreis;
		double bruttogesamtpreis;
		
		//Eingabe
		artikel = liesString(myScanner);
		anzahl = liesInt(myScanner);		
		nettopreis = liesDouble(myScanner);
		mwst = liesMwst(myScanner);
		
		//Verarbeitung
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);		
		
		//Ausgabe
		rechnungAusgabe(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
	}
		//Eingabe
	
		public static String liesString(Scanner myScanner){
			System.out.println("Was möchten Sie bestellen?");
			String artikel = myScanner.next();
			return artikel;
		}
		
		public static int liesInt(Scanner myScanner) {
			System.out.println("Geben Sie die Anzahl ein:");
			int anzahl = myScanner.nextInt();
			return anzahl;
		}
		
		public static double liesDouble(Scanner myScanner) {
			System.out.println("Geben Sie den Nettopreis ein:");
			double preis = myScanner.nextDouble();
			return preis;
		}
		
		public static double liesMwst(Scanner myScanner) {
			System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
			double mwst = myScanner.nextDouble();
			return mwst;
		}
		
		//Verarbeitung
		
		public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
			double n;
			n = anzahl * nettopreis;
			return n;
		}
			
		public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
			double g;
			g = nettogesamtpreis * (1 + mwst / 100);
			return g;
		}
		
		//Ausgabe
		
		public static void rechnungAusgabe(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
			System.out.println("\n\tRechnung:");
			System.out.printf("\t\t Netto: %-21s %6d %10.2f %s %n", artikel, anzahl, nettogesamtpreis, "€");
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f %s (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, "€", mwst, "%");
		}
		
	}

