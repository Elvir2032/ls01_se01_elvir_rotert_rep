﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
                         
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
      
    }
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag;
		double anzahlFahrkarten;
		
		System.out.print("Zu zahlender Betrag (EURO): ");
	    zuZahlenderBetrag = tastatur.nextDouble();
	      
	    System.out.print("Anzahl der Tickets: ");
	    anzahlFahrkarten = tastatur.nextByte(); //Byte: nur 1 Byte groß, ganzzahlig da Kommazahlen nicht benötigt werden, und maximal 127 Stellen
	    
	    return anzahlFahrkarten * zuZahlenderBetrag;
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double eingeworfeneMuenze;
		
		eingezahlterGesamtbetrag = 0.0;
	    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	      {
	       System.out.printf("%s%.2f%s%n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),  " Euro");
	       System.out.print("Eingabe (mind. 5 Ct, höchstens 2 Euro): ");
	       eingeworfeneMuenze = tastatur.nextDouble();
	       eingezahlterGesamtbetrag += eingeworfeneMuenze;
	      }
	    return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
				
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rueckgabebetrag > 0.0)
	       {
	    	   System.out.printf("%s%.2f%s%n", "Der Rückgabebetrag in Höhe von ", rueckgabebetrag, " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2,00 EURO");
		          rueckgabebetrag -= 2.0;
	           }
	           while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1,00 EURO");
		          rueckgabebetrag -= 1.0;
	           }
	           while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rueckgabebetrag -= 0.5;
	           }
	           while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rueckgabebetrag -= 0.2;
	           }
	           while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rueckgabebetrag -= 0.1;
	           }
	           while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rueckgabebetrag -= 0.05;
	           }
	           while(rueckgabebetrag >= 0.02)// 2 CENT-Münzen
	           {
	        	  System.out.println("2 CENT");
	 	          rueckgabebetrag -= 0.02;
	           }
	           while(rueckgabebetrag >= 0.01)// 1 CENT-Münzen
	           {
	        	  System.out.println("1 CENT");
	 	          rueckgabebetrag -= 0.01;
	           }
	           
	       }
	     
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	         
	}
	  
		
	}

