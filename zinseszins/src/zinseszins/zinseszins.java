package zinseszins;

import java.util.Scanner;

public class zinseszins {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Laufzeit (in Jahren) des Sparvertrages: ");
		int laufzeit = tastatur.nextInt();
		System.out.println("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		int kapital = tastatur.nextInt();
		System.out.println("Zinssatz: ");
		double zinssatz = tastatur.nextDouble();
				
		int laufzeitbase = 0;
		double zinskapital = 0;
		double endkapital  = kapital;
		
		while (laufzeit > laufzeitbase) {
			zinskapital = (endkapital * zinssatz) /100;
			endkapital = zinskapital + endkapital;
			laufzeit--;
		}
				
		System.out.println("Eingezahltes Kapital: " + kapital + " EURO");
		System.out.println("Ausgezahltes Kapital: " + endkapital + " EURO");
	
		
		

	}

}
