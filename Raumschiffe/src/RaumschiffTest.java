
public class RaumschiffTest {

	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		
		//r2.photonentorpedoSchiessen(r1);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		//r2.phaserkanoneSchiessen(r1);
		//r1.phaserkanoneSchiessen(r2);

		for (int i = 0; i < klingonen.eintraegeLogbuchZurueckgeben().size(); i++) {
			System.out.println(klingonen.eintraegeLogbuchZurueckgeben().get(i));
		}
				
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Roter Materie", 2));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedo", 3));
		
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.ladungsverzeichnisAusgeben();
		
				
		System.out.println("Klingonen: " + klingonen);
		System.out.println("Romulaner: " + romulaner);
		System.out.println("Vulkanier: " + vulkanier);
	}

}
