import java.util.ArrayList;

public class Raumschiff {
			
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
		
	}

	public void ladungsverzeichnisAusgeben() {
		for(int i= 0; i < ladungsverzeichnis.size(); i++) {
			System.out.print(this.ladungsverzeichnis.get(i).getBezeichnung() + " ");
			System.out.println(this.ladungsverzeichnis.get(i).getMenge());
		}
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		if(this.photonentorpedoAnzahl == 0) {
			System.out.println("-=*Click*=-");
		} else {
			nachrichtAnAlle("Photonentorpedo abgeschossen!");
			setPhotonentorpedoAnzahl(photonentorpedoAnzahl - 1);
			System.out.println("Photonentorpedoanzahl: " + photonentorpedoAnzahl);
			treffer(r);
		} 
	}

	public void phaserkanoneSchiessen(Raumschiff r) {
		if(this.energieversorgungInProzent < 50) {
			System.out.println("-=*Click*=-");
		} else {
			nachrichtAnAlle("Phaserkanone abgeschossen!");
			setEnergieversorgungInProzent(energieversorgungInProzent - 50);
			treffer(r);
		}
	}

	private void treffer(Raumschiff r) {
		nachrichtAnAlle(r.schiffsname + " wurde getroffen!");
		setSchildeInProzent(schildeInProzent - 50);
		if (r.schildeInProzent == 0) {
			this.setHuelleInProzent(huelleInProzent - 50);
			this.setEnergieversorgungInProzent(energieversorgungInProzent - 50);
			
		} if (r.huelleInProzent == 0) {
			this.setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent = 0);
			nachrichtAnAlle("Die Lebenserhaltungssysteme sind vollst�ndig vernichtet worden!");
		}
		else {
			nachrichtAnAlle("Nochmal Gl�ck gehabt!");
		}
		
	}
	
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}
	
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	
	public String toString() {
		return "[" + "photonentorpedoAnzahl = " + photonentorpedoAnzahl + ", energieversorgungInProzent = "
				+ energieversorgungInProzent + ", schildeInProzent = " + schildeInProzent + ", huelleInProzent = "
				+ huelleInProzent + ", lebenserhaltungssystemeInProzent = " + lebenserhaltungssystemeInProzent
				+ ", androidenAnzahl = " + androidenAnzahl + ", schiffsname = " + schiffsname + "]";
	}
	
	
}
