package lotto;


public class lotto {

	public static void main(String[] args) {
		
		int[] lottoZahlen = {3, 7, 12, 18, 37, 42};
					
		System.out.print("[");
		
		for (int i : lottoZahlen) 
			System.out.print(" " + i + " ");
		System.out.print("]");
		
		if(überprüfen(lottoZahlen, 12) == -1)
			System.out.println("\n Die Zahl " + überprüfen(lottoZahlen, 12) + " ist in der Ziehung nicht enthalten.");
		else
			System.out.println("\n Die Zahl " + überprüfen(lottoZahlen, 12) + " ist in der Ziehung enthalten.");
		
		if(überprüfen(lottoZahlen, 13) == -1)
			System.out.println("\n Die Zahl " + überprüfen(lottoZahlen, 13) + " ist in der Ziehung nicht enthalten.");
		else 
			System.out.println("\n Die Zahl " + überprüfen(lottoZahlen, 13) + " ist in der Ziehung enthalten.");
	}
	
	public static int überprüfen(int[] Liste, int zahl) {
			for(int i : Liste)
				if(i == zahl)
					return i;
			return -1;
	}

}


